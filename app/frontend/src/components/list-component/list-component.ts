import { Vue, Component } from 'vue-property-decorator'
import TaskElement from './../task-element/task-element.vue';
import TaskDto from './dto/task-dto';
import DataProviderinterface from './service/data-provider-interface';



@Component({name:'list-component', components: {TaskElement}})
export default class ListComponent extends Vue {
    $dataProvider!: DataProviderinterface;

    public tasks: Array<TaskDto> = [];
    public pages: number = 5;
    public currentPage: number = 1;
    public isLoaded: boolean = false;
    public sortField: string = 'id';
    public sortDirection: string = 'desc';
    public isAdmin:boolean = false;


    newName = '';
    newMail = '';
    newText = '';

    failMessage = '';

    created () {
        this.getData(1,'id','desc');
    }

    mounted () {
        this.$root.$on('notify-error',this.onNotify);
    }

    private onNotify(message:string)
    {
        this.failMessage = message;
    }

    public sortableClasses(state:string) {
        return {'fa': true,
        'fa-fw': true,
        'fa-sort-down': this.sortField === state && this.sortDirection === 'desc',
        'fa-sort-up': this.sortField === state && this.sortDirection === 'asc',
        'fa-sort': this.sortField !== state
        };
    }

    changeSort(field:string) {
        if (this.sortField === field){
            this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
        }
        this.getData(this.currentPage, field, this.sortDirection);
    }
    
    goTo(page:number) {
        this.getData(page,this.sortField, this.sortDirection);
    }

    createTask()
    {
        if (this.newName.trim().length === 0 || this.newName.trim().length === 0 || this.newName.trim().length === 0) {
            this.failMessage = 'Необходимо заполнить все данные!';    
            return;
        }
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(String(this.newMail).toLowerCase())){
            this.failMessage = 'Не валидный e-mail!';
            return;
        }
        this.isLoaded = false;

        const response = this.$dataProvider.addTask(this.newName,this.newMail,this.newText);
        response.then((result)=>{
            this.tasks = result.tasks;
            this.$root.$emit('ask-auth');
            this.pages = result.pages;
            this.currentPage = result.currentPage;
            this.sortField = result.sortField;
            this.sortDirection = result.sortDirection;
            this.isLoaded = true;
            this.failMessage = '';
            this.newName = '';
            this.newMail = '';
            this.newText = '';
        });

        response.catch(error => {
            this.isLoaded = true;
            this.failMessage = error.response.data.message;
        });
    }
    

    private getData(page:number,sortField:string, sortDirection: string) {
        this.isLoaded = false;
        this.tasks = [];
        const response =this.$dataProvider.getData(page,sortField,sortDirection);
            response.then((result)=>{
                this.tasks = result.tasks;
                this.pages = result.pages;
                this.currentPage = result.currentPage;
                this.sortField = result.sortField;
                this.sortDirection = result.sortDirection;
                this.isLoaded = true;
                this.$root.$emit('ask-auth');
            });

        
    }
}