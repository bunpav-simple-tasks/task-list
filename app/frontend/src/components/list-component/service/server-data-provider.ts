import axios from 'axios';
import DataProviderinterface from "./data-provider-interface";
import TaskDto from '../dto/task-dto';
import TaskResponseDto from "../dto/task-response-dto";
import qs from 'querystring'

export default class ServerDataProvider implements DataProviderinterface{
    getData(page:number, sortField:string, sortDirection:string): Promise<TaskResponseDto> {
        return axios.request<TaskResponseDto>({
            'url': '/api/v1/tasks/'+page+'/'+sortField+'/'+sortDirection+'/'
            })
            .then((response) => {
                const data = response.data;
                return new TaskResponseDto(data.tasks.map(TaskDto.create), data.pages, data.currentPage, data.sortField, data.sortDirection);
                // `data` is of type ServerData, correctly inferred
        });
    };

    addTask(author: string, email: string, message: string): Promise<TaskResponseDto> {
        return axios.request<TaskResponseDto>({
             'url':'/api/v1/tasks/'
            ,'method':"post"
            ,'headers': { 'content-type': 'application/x-www-form-urlencoded' }
            ,'data': qs.stringify({
                'author': author,
                'email': email,
                'message': message
            })
        }).then((response) => {
                const data = response.data;
                return new TaskResponseDto(data.tasks.map(TaskDto.create), data.pages, data.currentPage, data.sortField, data.sortDirection);
        });
    }

    updateTask(task: TaskDto): Promise<TaskDto> {
        const taskInfo = [task.id,task.author,task.email,task.message,task.status].join('/');
        return axios.request<TaskDto>({"url":'/api/v1/tasks/'+taskInfo+'/',"method":"put"})
            .then((response) => {
                const data = response.data;
                return TaskDto.create(data);
            });
    }



}