import TaskResponseDto from '../dto/task-response-dto';
import TaskDto from "../dto/task-dto";

export default interface DataProviderinterface {
    getData(page:number, sortField:string, sortDirection: string): Promise<TaskResponseDto>;

    addTask(author:string, email:string, message:string): Promise<TaskResponseDto>

    updateTask(task:TaskDto): Promise<TaskDto>
}