import TaskDto from "./task-dto";

export default class TaskResponseDto {
    private _tasks: Array<TaskDto> = [];
    private _pages: number = 1;
    private _currentPage: number = 1;
    private _sortField: string = '';
    private _sortDirection: string = '';

    constructor(tasks:Array<TaskDto>, pages:number, currentPage:number, sortField:string, sortDirection: string) {
        this._tasks = tasks;
        this._pages = pages;
        this._currentPage = currentPage;
        this._sortField = sortField;
        this._sortDirection = sortDirection;

    }

    public get tasks(): Array<TaskDto> {
        return this._tasks;
    }
    public set tasks(value: Array<TaskDto>) {
        this._tasks = value;
    }

    public get pages(): number {
        return this._pages;
    }

    public set pages(value: number){
        this._pages = value;
    }

    public get currentPage(): number {
        return this._currentPage;
    }

    public set currentPage(value: number){
        this._currentPage = value;
    }

    public get sortField(): string {
        return this._sortField;
    }

    public set sortField(value: string){
        this._sortField = value;
    }

    public set sortDirection(value: string){
        this._sortDirection = value;
    }

    public get sortDirection(): string {
        return this._sortDirection;
    }

}

