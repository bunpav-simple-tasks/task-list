export default class TaskDto {
    private _id: number;
    private _author: string;
    private _email: string;
    private _message: string;
    private _status: string;
    private _isEditedAdmin: boolean;

    constructor(id:number, author: string, email: string, message:string, status: string, isEditedAdmin: boolean)
    {
        this._id = id;
        this._author = author;
        this._email = email;
        this._message = message;
        this._status = status;
        this._isEditedAdmin = isEditedAdmin;

    }

    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }

    public get author(): string {
        return this._author;
    }
    public set author(value: string) {
        this._author = value;
    }

    public get email(): string {
        return this._email;
    }
    public set email(value: string) {
        this._email = value;
    }

    public get message(): string {
        return this._message;
    }
    public set message(value: string) {
        this._message = value;
    }
    public get status(): string {
        return this._status;
    }
    public set status(value: string) {
        this._status = value;
    }
    
    public get isEditedAdmin(): boolean {
        return this._isEditedAdmin;
    }
    public set isEditedAdmin(value: boolean) {
        this._isEditedAdmin = value;
    }

    public static create(input: any){
        return new TaskDto(
            input.id, input.author,input.email,input.message,input.status,input.isEditedAdmin
        );
    }
};