import { Vue, Component } from 'vue-property-decorator'
import AuthDataProvider from "./service/auth-data-provider";

@Component({name:'auth-component'})
export default class AuthComponent extends Vue {
    $authProvider!: AuthDataProvider;
    public state: string = 'closed';
    public failMessage = '';

    public login: string = '';
    public password: string = '';

    public isLogining: boolean = false;


    public logining() {
        const api = this.$authProvider.login(this.login, this.password);

        api.then( (isAdmin) => {
            this.$root.$emit('authorize', isAdmin);
            this.isLogining = true;
            this.failMessage = '';
            this.state = 'closed';
        });

        api.catch( (failResult) => {
            this.failMessage = 'Доступ запрещён! Не коррктные реквизиты доступа';
            this.$root.$emit('authorize', false);
        });
    }

    created(){
        this.checkAdmin();

        setTimeout(()=> {
            this.$root.$emit('authorize', this.isLogining);
        }, 150);
    }

    mounted () {
        this.$root.$on('ask-auth',this.checkAdmin);
    }

    private checkAdmin()
    {
        const api = this.$authProvider.isAdmin();

        api.then((isAdmin)=>{
            this.$root.$emit('authorize', isAdmin);
            this.failMessage = '';
            this.state = 'closed';
            this.isLogining = isAdmin;
        });

        api.catch( (failResult) => {
            this.$root.$emit('authorize', false);
        });
    }

    public close(){
        this.login = '';
        this.password ='';
        this.failMessage = '';
        this.state = 'closed';
    }

    public openLogin() {
        this.state = 'open';
    }

    public logout() {
        const api = this.$authProvider.logout();
        api.then((isAdmin)=>{
            this.$root.$emit('authorize', isAdmin);
            this.$root.$emit('notify-error', '');
            this.failMessage = '';
            this.isLogining = isAdmin;
        });
    }


    public popupState(){
        return {'modal': true,
            'show': this.state === 'open',
            'd-block': this.state === 'open'
        };
    }
}