import axios from 'axios';
import qs from "querystring";

export default class AuthDataProvider {

    isAdmin(): Promise<boolean> {
        const api = axios.get('/api/v1/user/');

        return api.then((response) => {
            return response.data.isAdmin
        });
    }

    login(login: string, password: string): Promise<boolean> {
        const api = axios.post('/api/v1/user/login/',qs.stringify({
            'login': login,
            'password': password
        }),{
            'headers': { 'content-type': 'application/x-www-form-urlencoded' }
        });

        return api.then((response) => {
            return response.data.isAdmin
        });
    }

    logout(): Promise<boolean> {
        const api = axios.post('/api/v1/user/logout/');

        return api.then((response) => {
            return response.data.isAdmin
        });
    }
}