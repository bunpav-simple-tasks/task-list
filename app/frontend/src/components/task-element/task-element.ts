import { Vue, Component, Prop, Watch } from 'vue-property-decorator'
import DataProviderinterface from "../list-component/service/data-provider-interface";
import TaskDto from "../list-component/dto/task-dto";

@Component({name:'task-element'})
export default class TaskElement extends Vue {
    $dataProvider!: DataProviderinterface;

    @Prop() id!: number;
    @Prop() author!: string;
    @Prop() email!: string;
    @Prop() message!: string;
    @Prop() status!: string;
    @Prop() isEditedAdmin!: boolean;

    public isAdmin: boolean = false;

    public isEditing:boolean = false;

    _message = '';
    _status = '';
    _isEditedAdmin = false;

    beforeCreate () {

    }

    created () {
      this._message = this.message;
      this._status = this.status;
      this._isEditedAdmin = this.isEditedAdmin;
    }

    @Watch('message')
    onMessagePropChange(val: string) {
        this._message = val;
    }
    @Watch('isEditedAdmin')
    onAdminPropChange(val: boolean) {
        this._isEditedAdmin = val;
    }

    editMode () {
      this.isEditing = true;
    }

    public updateField() {
        const api = this.$dataProvider.updateTask(
            new TaskDto(this.id,this.author,this.email,this._message,this._status,this.isEditedAdmin)
        );
        this.$root.$emit('notify-error', '');
        api.then((taskDto: TaskDto)=>{
            this._isEditedAdmin = taskDto.isEditedAdmin;
            this.isEditing = false;
        }).catch((error) => {
            this.$root.$emit('notify-error', error.response.data.message);
        });
    }

    mounted () {
        this.$root.$on('authorize',this.onAuthorize);
    }

    onAuthorize(isAdmin:boolean)
    {
        this.isAdmin = isAdmin;
        this.isEditing = false;
    }
}
