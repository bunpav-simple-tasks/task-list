import Vue from 'vue'
import App from './App.vue';
import ServerDataProvider from './components/list-component/service/server-data-provider';
import AuthDataProvider from "./components/auth-component/service/auth-data-provider";

Vue.prototype.$dataProvider = new ServerDataProvider;
Vue.prototype.$authProvider = new AuthDataProvider;

const app = {
  render(h:any) {
    return h(App);
  }
};

new Vue(app).$mount('#app');
