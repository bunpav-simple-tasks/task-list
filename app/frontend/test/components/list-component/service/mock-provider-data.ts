import DataProviderInterface from './../../../../src/components/list-component/service/data-provider-interface';
import TaskDto from '../../../../src/components/list-component/dto/task-dto';
import TaskResponseDto from '../../../../src/components/list-component/dto/task-response-dto';

export default class MockProviderData implements DataProviderInterface
{
    getData(page:number, sortField:string): Promise<TaskResponseDto> {
        return new Promise((resolve, reject) => {
            resolve(new TaskResponseDto([new TaskDto(1,'author-1','2ss@mail.ru','some-text','Создано',false),
                new TaskDto(2,'author-1','3ss@mail.ru','some-text','Создано',true),
                new TaskDto(3,'author-1','4ss@mail.ru','some-text','Выполнено',false)
        ],5,page,sortField,'desc'));
        });
     }

    addTask(author: string, email: string, message: string): Promise<TaskResponseDto> {
        return new Promise((resolve, reject) => {
            resolve(new TaskResponseDto([new TaskDto(1,'author-1','2ss@mail.ru','some-text','Создано',false),
                new TaskDto(2,'author-1','3ss@mail.ru','some-text','Создано',true),
                new TaskDto(3,'author-1','4ss@mail.ru','some-text','Выполнено',false)
            ],5,1,'id','desc'));
        });
    }

    updateTask(task: TaskDto): Promise<TaskDto> {
        return new Promise((resolve, reject) => {
            task.isEditedAdmin = true;
            resolve(task);
        });
    }
}