import 'jest';
import { shallowMount } from '@vue/test-utils';
import TaskElement from "../../../src/components/task-element/task-element";


const OPTIONS = {
    propsData: {
        author: 'author',
        email: 'some@email.ru',
        message: 'simple task',
        status: 'Создано'
    }
};

describe('task-element.vue', () => {
    test('Отрисовывает элемент задачи', () => {
        const wrapper = shallowMount(TaskElement, OPTIONS);
        wrapper.vm.$data.isAdmin = true;
        expect(wrapper.text()).toMatch('author some@email.ru simple task  Создано Правка');
    });

    test('Клик на "Правка" меняет стейт', () => {
        const wrapper = shallowMount(TaskElement, OPTIONS);
        wrapper.vm.$data.isAdmin = true;
        const edit = wrapper.find('button[type="button"]');
        edit.trigger('click');
        expect(wrapper.text()).toMatch('author some@email.ru   Создано В процессе Выполнено Сохранить');
    });

    test('Обновление элемента', () => {
        const wrapper = shallowMount(TaskElement, OPTIONS);
        wrapper.vm.$data.isAdmin = true;
        const edit = wrapper.find('button[type="button"]');
        edit.trigger('click');
        wrapper.find('input').setValue('new message');
        wrapper.find('select').setValue('Выполнено');
        wrapper.vm.updateField = jest.fn();
        wrapper.vm._isEditedAdmin = true;
        edit.trigger('click');
        expect(wrapper.text()).toMatch('author some@email.ru new message  Выполнено Правка');
    });

    test('Присутствует надпись "отредактировано администратором"', () => {
        const newOptions = { propsData : Object.assign(OPTIONS.propsData, {isEditedAdmin: true})};
        const wrapper = shallowMount(TaskElement, newOptions);  
        expect(wrapper.text()).toMatch(/отредактировано администратором/);
    });



});