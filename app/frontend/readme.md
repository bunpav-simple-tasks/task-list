Скелет Vue + TypeScript + Unit 
===

- ```npm i ```  - установить зависимости
- ```npm run build ``` - собрать приложение
- ```npm run test:lint ``` - линтер, нет замечаний по стилю
- ```npm run test:unit ``` - тест, апп работает как закладывали
- ```npm run start:dev``` - запускаем дев. сервер (порт: 8085)