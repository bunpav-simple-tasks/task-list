<?php

namespace App\Core;

abstract class BaseComponent
{
    /**
     * @return string
     */
    abstract public function getConfigName(): string;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->configureParameters($this->getConfig());
    }

    public function configureParameters(array $config)
    {
        if (!empty($config)) {
            foreach ($config as $name => $value) {
                $this->$name = $value;
            }
        }
    }

    public function getConfig()
    {
        if (!empty($this->getConfigName())) {
            $configPathMath = explode('.', $this->getConfigName(), 2);
            $file = $configPathMath[0] ?? null;
            $key = $configPathMath[1] ?? null;

            $config = require CONFIG_PATH . $file . ".php";

            if (empty($key)) {
                return $config;
            } else {
                return $config[$key] ?? [];
            }
        }
        return [];
    }
}
