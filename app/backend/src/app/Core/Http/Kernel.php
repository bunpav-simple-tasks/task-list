<?php

namespace App\Core\Http;

use App\Core\KernelInterface;
use App\Core\BaseComponent;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\ResponseFactory;
use Zend\Diactoros\ServerRequestFactory;
use Aura\Router\RouterContainer;

abstract class Kernel extends BaseComponent implements KernelInterface
{
    protected $responseFactory;
    protected $requestFactory;

    /**
     * @var RouterContainer
     */
    protected $router;

    public function __construct(ResponseFactory $responseFactory, ServerRequestFactory $serverRequestFactory)
    {
        parent::__construct();
        $this->responseFactory = $responseFactory;
        $this->requestFactory = $serverRequestFactory;
        $this->registerRouter();
    }

    public function getConfigName(): string
    {
        return "app";
    }

    /**
     * Выполняет запуск приложения
     */
    public function run()
    {
        $response = $this->handleRequest();
        foreach ($response->getHeaders() as $name => $values) {
            foreach ($values as $value) {
                header(sprintf('%s: %s', $name, $value), false);
            }
        }

        http_response_code($response->getStatusCode());

        echo $response->getBody();
    }

    /**
     * Обработчик запросов
     * @return ResponseInterface
     */
    abstract protected function handleRequest(): ResponseInterface;

    /**
     * Регистратор роутера
     */
    abstract protected function registerRouter(): void;
}
