<?php

namespace App\Core;

interface KernelInterface
{
    public function run();
}
