<?php
namespace App\Core;

use PDO;

class DataBaseComponent extends BaseComponent
{
    /** @var \PDO */
    private $pdo;

    /** @var string */
    protected $user;
    /** @var string */
    protected $password;
    /** @var string */
    protected $database;
    /** @var string */
    protected $host;
    /** @var int */
    protected $port;

    public function getConfigName(): string
    {
        return 'database';
    }

    public function __construct()
    {
        parent::__construct();

        $this->initialize();
    }

    private function initialize()
    {
        $this->pdo = new PDO(vsprintf('pgsql:host=%s;dbname=%s', [
            $this->host, $this->database
            ]), $this->user, $this->password);
    }

    public function getConnection(): PDO
    {
        return $this->pdo;
    }
}
