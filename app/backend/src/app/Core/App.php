<?php

namespace App\Core;

use Psr\Container\ContainerInterface;
use App\Core\KernelInterface;

class App
{
    protected static $kernel;

    protected static $container;

    public static function getContainer(): ContainerInterface
    {
        return self::$container;
    }

    public static function setContainer(ContainerInterface $container): void
    {
        self::$container = $container;
    }

    public static function getKernel(): KernelInterface
    {
        return self::$kernel;
    }

    public static function setKernel(KernelInterface $kernel): void
    {
        self::$kernel = $kernel;
    }
}
