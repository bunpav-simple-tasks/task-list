<?php

namespace App\Http\Middleware;

use App\Service\Auth\AuthRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

class AuthorizeMiddleware implements MiddlewareInterface
{
    private $authService;

    public function __construct(AuthRepository $service)
    {
        $this->authService = $service;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $nextContainerHandler): ResponseInterface
    {

        if (empty($_SESSION['user'])) {
            return new JsonResponse(['message'=>'Необходима авторизация!'], 403);
        }

        return new JsonResponse([], 200);
    }
}
