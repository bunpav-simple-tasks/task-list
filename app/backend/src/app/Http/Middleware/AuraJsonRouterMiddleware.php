<?php

namespace App\Http\Middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

use Aura\Router\RouterContainer;
use Zend\Diactoros\Response\JsonResponse;

class AuraJsonRouterMiddleware implements MiddlewareInterface
{
    private $routerContainer;
    private $container;

    public function __construct(RouterContainer $routerContainer, ContainerInterface $container)
    {
        $this->routerContainer = $routerContainer;
        $this->container = $container;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $nextContainerHandler): ResponseInterface
    {
        $matcher = $this->routerContainer->getMatcher();
        $route = $matcher->match($request);
        if (! $route) {
            return new JsonResponse(['message'=>'No route found for the request.'], 404);
        }

        // add route attributes to the request
        foreach ($route->attributes as $key => $val) {
            $request = $request->withAttribute($key, $val);
        }

        $authMiddleware = $route->auth;
        if (!empty($authMiddleware) && class_exists($authMiddleware)) {
            $auth = $this->container->make($authMiddleware);
            if ($auth instanceof MiddlewareInterface) {
                $preResponse = $auth->process($request, $nextContainerHandler);
                if ($preResponse->getStatusCode() === 403) {
                    return $preResponse;
                }
            }
        }

        // dispatch the request to the route handler.
        // (consider using https://github.com/auraphp/Aura.Dispatcher
        // in place of the one callable below.)
        $callable = $route->handler;
        $response = $callable($request);
        return $response;
    }
}
