<?php
namespace App\Http\Controllers;

use App\Service\Auth\AuthRepository;
use App\Service\Auth\Dto\UserDto;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class UserController extends Controller
{
    private $service;

    public function __construct(AuthRepository $service)
    {
        $this->service = $service;
    }

    public function getUser(ServerRequestInterface $serverRequest)
    {
        $userDto = $_SESSION['user'] ?: new UserDto();
        return new JsonResponse($userDto);
    }

    public function login(ServerRequestInterface $serverRequest)
    {
        $userDto = new UserDto();
        $input = $serverRequest->getParsedBody();

        if (empty($input['login']) || empty($input['password'])) {
            return new JsonResponse($userDto, 403);
        }

        $userDb = $this->service->getUser($input['login']);

        if (empty($userDb)) {
            return new JsonResponse($userDto, 403);
        }

        if (!$this->service->checkPassword($input['password'], $userDb->getPassword())) {
            return new JsonResponse($userDto, 403);
        }

        $_SESSION['user'] = $userDb;

        return new JsonResponse($userDb->setPassword(''));
    }

    public function logout(ServerRequestInterface $serverRequest)
    {
        unset($_SESSION['user']);
        return new JsonResponse(new UserDto());
    }

    public function createUser(ServerRequestInterface $serverRequest)
    {
        $input = $serverRequest->getParsedBody();

        if (empty($input['email']) || empty($input['password'])) {
            return new JsonResponse(['message'=>'Все данные должны быть заполнены корректно', 400]);
        }

        $user = new UserDto();
        $user->setPassword(password_hash($input['password'], PASSWORD_ARGON2ID, ['cost'=>7]))->setLogin($input['email']);

        return new JsonResponse($this->service->addUser($user), 201);
    }
}
