<?php

namespace App\Http\Controllers;

use App\Service\Task\Contract\TaskServiceInterface;
use App\Service\Task\Dto\FilterDto;
use App\Service\Task\Dto\TaskDto;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\JsonResponse;

class TaskController extends Controller
{

    /**
     * @var TaskServiceInterface
     */
    private $taskService;

    public function __construct(TaskServiceInterface $service)
    {
        $this->taskService = $service;
    }

    public function addTask(ServerRequestInterface $request)
    {
        $input = $request->getParsedBody();
        if (empty($input['author']) || empty($input['message']) || !filter_var($input['email'], FILTER_VALIDATE_EMAIL)) {
            return new JsonResponse(['message'=>'Все данные должны быть заполнены корректно'], 400);
        }

        $task = new TaskDto();
        $task->setAuthor($input['author'])
            ->setEmail($input['email'])
            ->setMessage($input['message'])
            ;
        $this->taskService->addTask($task);

        $listing = $this->taskService->getTaskList((new FilterDto())
            ->setSortField('id')
            ->setSortDirection('desc')
            ->setShow(3)
            ->setPage(1));

        return new JsonResponse($listing, 201);
    }

    public function updateTask(ServerRequestInterface $request)
    {
        $author = trim($request->getAttribute('author') ?: '');
        $email = trim($request->getAttribute('email') ?: '');
        $message = trim($request->getAttribute('message') ?: '');
        $id = (int) trim($request->getAttribute('id') ?: '');
        $status = trim($request->getAttribute('status') ?: '');


        if (empty($author)
            || empty($message)
            || !filter_var($email, FILTER_VALIDATE_EMAIL)
            || empty($id)
            || !in_array($status, ['Создано','В процессе', 'Выполнено'])
        ) {
            return new JsonResponse(['message'=>'Все данные должны быть заполнены корректно'], 400);
        }

        $task = new TaskDto();
        $task->setId($id)
             ->setMessage($message)
             ->setEmail($email)
             ->setAuthor($author)
             ->setStatus($status)
        ;

        return new JsonResponse($this->taskService->updateTask($task), 202);
    }


    public function getTaskList(ServerRequestInterface $request)
    {
        $filter = new FilterDto();
        $filter->setPage((int) $request->getAttribute('page', 1) ?: 1)
               ->setShow(3)
               ->setSortField($request->getAttribute('field', 'id') ?: 'id')
               ->setSortDirection($request->getAttribute('sort', 'desc') ?: 'desc');

        return new JsonResponse($this->taskService->getTaskList($filter));
    }
}
