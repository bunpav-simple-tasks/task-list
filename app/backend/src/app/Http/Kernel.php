<?php
namespace App\Http;

use App\Core\Http\Kernel as HttpKernel;
use App\Core\App;

use Psr\Http\Message\ResponseInterface;
use Aura\Router\RouterContainer;
use Equip\Dispatch\MiddlewareCollection;

class Kernel extends HttpKernel
{
    protected function handleRequest(): ResponseInterface
    {
        $middleware = [
            new Middleware\StartSessionMiddleware(),
            new Middleware\AuraJsonRouterMiddleware($this->router, App::getContainer()),
        ];
        
        $collection = new MiddlewareCollection($middleware);
        
        $defaultResponse = [$this->responseFactory,'createResponse'];
        // Правильнее было бы используя PSR создать методом createResponse
        $request = $this->requestFactory::fromGlobals(
            $_SERVER,
            $_GET,
            $_POST,
            $_COOKIE,
            $_FILES
        );
        
        $response =  $collection->dispatch($request, $defaultResponse);

        return $response;
    }

    protected function registerRouter(): void
    {
        $this->router = App::getContainer()->make(RouterContainer::class);
        $map = $this->router->getMap();
        require ROUTE_PATH.'/api.php';
    }
}
