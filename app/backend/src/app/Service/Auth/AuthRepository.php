<?php
namespace App\Service\Auth;

use App\Core\DataBaseComponent;
use App\Service\Auth\Dto\UserDto;
use PDO;

class AuthRepository
{
    /** @var  PDO*/
    private $db;

    public function __construct(DataBaseComponent $db)
    {
        $this->db = $db->getConnection();
    }

    public function addUser(UserDto $userDto): UserDto
    {
        $sql = 'INSERT INTO users ("login","password") '.
               'VALUES (:login, :password)';

        $command = $this->db->prepare($sql);
        $command->bindValue(':login', $userDto->getLogin(), PDO::PARAM_STR);
        $command->bindValue(':password', $userDto->getPassword(), PDO::PARAM_STR);
        $command->execute();

        $userDto->setPassword('')->setIsAdmin(true);

        return $userDto;
    }

    public function getUser(string $login): ?UserDto
    {
        $sql = 'SELECT id, login, password, true as "isAdmin" FROM users where login=:login';
        $command = $this->db->prepare($sql);
        $command->bindValue(':login', $login, PDO::PARAM_STR);
        $command->execute();
        /** @var  UserDto $user */
        $user = $command->fetchObject(UserDto::class);
        return $user ?: null;
    }

    public function checkPassword(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}
