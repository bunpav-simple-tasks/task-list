<?php
namespace App\Service\Auth\Dto;

use App\Service\Traits\JsonSerializeTrait;

class UserDto implements \JsonSerializable
{
    use JsonSerializeTrait;

    /** @var  int */
    protected $id;
    /** @var string */
    protected $login;
    /** @var  bool */
    protected $isAdmin = false;
    /** @var string */
    private $password;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UserDto
     */
    public function setId(int $id): UserDto
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return UserDto
     */
    public function setLogin(string $login): UserDto
    {
        $this->login = $login;
        return $this;
    }



    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    /**
     * @param bool $isAdmin
     * @return UserDto
     */
    public function setIsAdmin(bool $isAdmin): UserDto
    {
        $this->isAdmin = $isAdmin;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return UserDto
     */
    public function setPassword(string $password): UserDto
    {
        $this->password = $password;
        return $this;
    }
}
