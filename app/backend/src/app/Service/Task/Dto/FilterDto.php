<?php

namespace App\Service\Task\Dto;

class FilterDto
{
    /**
     * По какому полю фильтруем
     *
     * @var string
     */
    protected $sortField;
    /**
     * Направление сортировки
     *
     * @var string ASC|DESC
     */
    protected $sortDirection;

    /**
     * Какую страницу показать
     *
     * @var int
     */
    protected $page;

    /**
     * Записей на странице
     *
     * @var int
     */
    protected $show;

    /**
     * @return string
     */
    public function getSortField(): string
    {
        return $this->sortField;
    }

    /**
     * @param string $sortField
     * @return FilterDto
     */
    public function setSortField(string $sortField): FilterDto
    {
        $this->sortField = $sortField;
        return $this;
    }

    /**
     * @return string
     */
    public function getSortDirection(): string
    {
        return $this->sortDirection;
    }

    /**
     * @param string $sortDirection
     * @return FilterDto
     */
    public function setSortDirection(string $sortDirection): FilterDto
    {
        $this->sortDirection = $sortDirection;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return FilterDto
     */
    public function setPage(int $page): FilterDto
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getShow(): int
    {
        return $this->show;
    }

    /**
     * @param int $show
     * @return FilterDto
     */
    public function setShow(int $show): FilterDto
    {
        $this->show = $show;
        return $this;
    }
}
