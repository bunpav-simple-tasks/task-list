<?php

namespace App\Service\Task\Dto;

use App\Service\Traits\JsonSerializeTrait;

class TaskDto implements \JsonSerializable
{
    use JsonSerializeTrait;
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $author;
    /**
     * @var string
     */
    protected $email;
    /**
     * @var string
     */
    protected $message;
    /**
     * @var string
     */
    protected $status;
    /**
     * @var bool
     */
    protected $isEditedAdmin;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TaskDto
     */
    public function setId(int $id): TaskDto
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return TaskDto
     */
    public function setAuthor(string $author): TaskDto
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return TaskDto
     */
    public function setEmail(string $email): TaskDto
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return TaskDto
     */
    public function setMessage(string $message): TaskDto
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return TaskDto
     */
    public function setStatus(string $status): TaskDto
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEditedAdmin(): bool
    {
        return $this->isEditedAdmin;
    }

    /**
     * @param bool $isEditedAdmin
     * @return TaskDto
     */
    public function setIsEditedAdmin(bool $isEditedAdmin): TaskDto
    {
        $this->isEditedAdmin = $isEditedAdmin;
        return $this;
    }


    public function loadFromArray($array): TaskDto
    {
        $task = new self();
        $task->setId($array['id'])
             ->setStatus($array['status'])
             ->setMessage($array['message'])
             ->setEmail($array['email'])
             ->setAuthor($array['author'])
             ->setIsEditedAdmin(filter_var($array['is_edited_admin'], \FILTER_VALIDATE_BOOLEAN));

        return $task;
    }
}
