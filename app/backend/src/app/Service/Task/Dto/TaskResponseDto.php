<?php

namespace App\Service\Task\Dto;

use App\Service\Traits\JsonSerializeTrait;

class TaskResponseDto implements \JsonSerializable
{
    use JsonSerializeTrait;

    /** @var TaskDto[] */
    protected $tasks;
    /** @var int */
    protected $pages;
    /** @var int */
    protected $currentPage;
    /** @var string */
    protected $sortField;
    /** @var string */
    protected $sortDirection;

    /**
     * @return TaskDto[]
     */
    public function getTasks(): array
    {
        return $this->tasks;
    }

    /**
     * @param TaskDto[] $tasks
     * @return TaskResponseDto
     */
    public function setTasks(array $tasks): TaskResponseDto
    {
        $this->tasks = $tasks;
        return $this;
    }

    /**
     * @return int
     */
    public function getPages(): int
    {
        return $this->pages;
    }

    /**
     * @param int $pages
     * @return TaskResponseDto
     */
    public function setPages(int $pages): TaskResponseDto
    {
        $this->pages = $pages;
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     * @return TaskResponseDto
     */
    public function setCurrentPage(int $currentPage): TaskResponseDto
    {
        $this->currentPage = $currentPage;
        return $this;
    }

    /**
     * @return string
     */
    public function getSortField(): string
    {
        return $this->sortField;
    }

    /**
     * @param string $sortField
     * @return TaskResponseDto
     */
    public function setSortField(string $sortField): TaskResponseDto
    {
        $this->sortField = $sortField;
        return $this;
    }

    /**
     * @return string
     */
    public function getSortDirection(): string
    {
        return $this->sortDirection;
    }

    /**
     * @param string $sortDirection
     * @return TaskResponseDto
     */
    public function setSortDirection(string $sortDirection): TaskResponseDto
    {
        $this->sortDirection = $sortDirection;
        return $this;
    }
}
