<?php


namespace App\Service\Task;

use App\Service\Task\Contract\TaskServiceInterface;
use App\Service\Task\Dto\FilterDto;
use App\Service\Task\Dto\TaskDto;
use App\Service\Task\Dto\TaskResponseDto;

class TaskService implements Contract\TaskServiceInterface
{
    /**
     * @var TaskServiceInterface
     */
    private $repository;

    public function __construct(Contract\TaskServiceInterface $repository)
    {
        $this->repository =$repository;
    }

    public function addTask(TaskDto $task): ?TaskDto
    {
        return $this->repository->addTask($task);
    }

    public function updateTask(TaskDto $task): ?TaskDto
    {
        return $this->repository->updateTask($task);
    }

    public function getTaskList(FilterDto $filter): TaskResponseDto
    {
        $result = $this->repository->getTaskList($filter);
        $result->setSortDirection($filter->getSortDirection());
        $result->setSortField($filter->getSortField());
        $result->setCurrentPage($filter->getPage());

        return $result;
    }
}
