<?php

namespace App\Service\Task\Contract;

use App\Service\Task\Dto\TaskDto;
use App\Service\Task\Dto\FilterDto;
use App\Service\Task\Dto\TaskResponseDto;

interface TaskServiceInterface
{
    /**
     * Создаёт новую задачу
     *
     * @param TaskDto $task
     * @return TaskDto|null
     */
    public function addTask(TaskDto $task): ?TaskDto;

    /**
     * Обновляет переданную задачу
     *
     * @param TaskDto $task
     * @return TaskDto|null
     */
    public function updateTask(TaskDto $task): ?TaskDto;

    /**
     * Возвращает список задач
     *
     * @param FilterDto $filter
     * @return TaskResponseDto
     */
    public function getTaskList(FilterDto $filter): TaskResponseDto;
}
