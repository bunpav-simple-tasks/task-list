<?php

namespace App\Service\Task;

use App\Core\DataBaseComponent;
use App\Service\Task\Dto\TaskDto;
use App\Service\Task\Dto\FilterDto;
use App\Service\Task\Dto\TaskResponseDto;
use PDO;

class TaskRepository implements Contract\TaskServiceInterface
{
    /** @var PDO  */
    private $db;

    public function __construct(DataBaseComponent $db)
    {
        $this->db = $db->getConnection();
    }
    /**
     * {@inheritDoc}
     */
    public function addTask(TaskDto $task): ?TaskDto
    {
        $sql = 'INSERT INTO tasks ("author", "email", "message") '.
               'VALUES (:author, :email, :message) '.
               'RETURNING id';
        $command = $this->db->prepare($sql);
        $command->bindValue(':author', $task->getAuthor(), PDO::PARAM_STR);
        $command->bindValue(':email', $task->getEmail(), PDO::PARAM_STR);
        $command->bindValue(':message', $task->getMessage(), PDO::PARAM_STR);
        $command->execute();
        $result = $command->fetch(PDO::FETCH_ASSOC);

        $task->setId($result['id'])
            ->setStatus('Создано')
            ->setIsEditedAdmin(false)
        ;

        return $task;
    }

    /**
     * {@inheritDoc}
     */
    public function updateTask(TaskDto $task): ?TaskDto
    {
        $sql = 'UPDATE tasks set
                "message" = :message
                , "status" =  :status
                , "is_edited_admin" = ("message" != :update_message OR is_edited_admin)'.
               'WHERE id = :id '.
               'RETURNING is_edited_admin';
        $command = $this->db->prepare($sql);
        $command->bindValue(':message', $task->getMessage(), \PDO::PARAM_STR);
        $command->bindValue(':status', $task->getStatus(), \PDO::PARAM_STR);
        $command->bindValue(':update_message', $task->getMessage(), \PDO::PARAM_STR);
        $command->bindValue(':id', $task->getId(), \PDO::PARAM_INT);
        $command->execute();

        $task->setIsEditedAdmin($command->fetch(PDO::FETCH_ASSOC)['is_edited_admin']);

        return $task;
    }

    /**
     * {@inheritDoc}
     */
    public function getTaskList(FilterDto $filter): TaskResponseDto
    {
        $result = new TaskResponseDto();
        $skip = ($filter->getPage() - 1) * $filter->getShow();

        $listingSql = sprintf('SELECT id, author, email, message, status, is_edited_admin '.
                      'FROM tasks '.
                      'order by %s %s '.
                      'limit :pageSize offset :skip', $filter->getSortField(), $filter->getSortDirection()) ;

        $command = $this->db->prepare($listingSql);
        $command->bindValue(':pageSize', $filter->getShow(), \PDO::PARAM_INT);
        $command->bindValue(':skip', $skip, \PDO::PARAM_INT);
        $command->execute();
        $listing = $command->fetchAll(PDO::FETCH_ASSOC);

        $result->setTasks(array_map([new TaskDto(), 'loadFromArray'], $listing))
               ->setPages($this->getPageCount($filter->getShow()));


        return $result;
    }

    private function getPageCount(int $pageSize):int
    {
        $sql = "select concat(count(*) / :pageSize,'.', count(*) % :pageSize) as pages FROM tasks";
        $command = $this->db->prepare($sql);
        $command->bindValue(':pageSize', $pageSize, \PDO::PARAM_INT);
        $command->execute();
        return (int) ceil($command->fetch(PDO::FETCH_ASSOC)['pages']);
    }
}
