<?php

namespace App\Service\Traits;

trait JsonSerializeTrait
{
    /**
     * Возвращает структуру объекта для json представления
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
