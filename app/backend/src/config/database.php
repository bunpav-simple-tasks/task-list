<?php
return [
  'host' => getenv('APP_DB_HOST', true),
  'port' => getenv('APP_DB_PORT', true),
  'database' => getenv('APP_DB_DATABASE', true),
  'user' => getenv('APP_DB_USERNAME', true),
  'password' => getenv('APP_DB_PASSWORD', true),
];
