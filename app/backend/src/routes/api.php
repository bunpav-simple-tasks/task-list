<?php
use App\Core\App;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;

$taskController = App::getContainer()->make(TaskController::class);
$userController = App::getContainer()->make(UserController::class);
/** @var \Aura\Router\Map $map */
$map->get('task.list', '/api/v1/tasks{/page,field,sort}/', [$taskController,'getTaskList'])
    ->tokens([
        'page' => '\d',
        'field'=> '(id|author|email|status)',
        'sort'=> '(asc|desc)'
    ]);

$map->post('task.add', '/api/v1/tasks/', [$taskController,'addTask']);


$map->put('task.update', '/api/v1/tasks/{id}/{author}/{email}/{message}/{status}/', [$taskController,'updateTask'])
    ->auth(\App\Http\Middleware\AuthorizeMiddleware::class);


$map->get('user.get', '/api/v1/user/', [$userController,'getUser']);
$map->post('user.login', '/api/v1/user/login/', [$userController,'login']);
$map->post('user.logout', '/api/v1/user/logout/', [$userController,'logout']);
