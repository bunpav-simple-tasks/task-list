<?php
defined('BASE_PATH') or define('BASE_PATH', dirname(__DIR__) . "/app/");
defined('ROUTE_PATH') or define('ROUTE_PATH', dirname(__DIR__) . "/routes/");

require BASE_PATH . 'Core/App.php';

defined('CONFIG_PATH') or define('CONFIG_PATH', dirname(__DIR__) . "/config/");

$container = new \DI\Container();
$container->set(App\Service\Task\Contract\TaskServiceInterface::class, DI\create(App\Service\Task\TaskRepository::class)
    ->constructor(DI\get(App\Core\DataBaseComponent::class)));
$container->set(App\Service\Task\TaskService::class, DI\create(App\Service\Task\TaskService::class)
    ->constructor(DI\get(App\Service\Task\Contract\TaskServiceInterface::class)));
$container->set(App\Http\Controllers\TaskController::class, DI\create(App\Http\Controllers\TaskController::class)
                    ->constructor(DI\get(App\Service\Task\TaskService::class)));

App\Core\App::setContainer($container);
App\Core\App::setKernel(App\Core\App::getContainer()->make(App\Http\Kernel::class));
