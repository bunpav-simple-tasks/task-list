<?php
require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../src/bootstrap/app.php';
\App\Core\App::getKernel()->run();
/**
use Equip\Dispatch\MiddlewareCollection;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class FooMiddleware implements MiddlewareInterface {
    public function process(ServerRequestInterface $request, RequestHandlerInterface $nextContainerHandler): ResponseInterface
    {
        echo __CLASS__;
        return $nextContainerHandler->handle($request);
    }
}

class BarMiddleware implements MiddlewareInterface {
    public function process(ServerRequestInterface $request, RequestHandlerInterface $nextContainerHandler): ResponseInterface
    {
        echo __CLASS__;
        $responce = new Zend\Diactoros\Response\JsonResponse(['ss']);
        return $responce;
    }
}

class AuroraRouterMiddleware implements MiddlewareInterface {
    private $routerContainer;
    public function __construct(Aura\Router\RouterContainer $routerContainer)
    {
        $this->routerContainer = $routerContainer;
    }
    public function process(ServerRequestInterface $request, RequestHandlerInterface $nextContainerHandler): ResponseInterface
    {
        $matcher = $this->routerContainer->getMatcher();
        $route = $matcher->match($request);
        if (! $route) {
            echo "No route found for the request.";
            exit;
        }
        // add route attributes to the request

        foreach ($route->attributes as $key => $val) {
            $request = $request->withAttribute($key, $val);
        }

        // dispatch the request to the route handler.
        // (consider using https://github.com/auraphp/Aura.Dispatcher
        // in place of the one callable below.)
        $callable = $route->handler;
        $response = $callable($request);
        return $response;
    }
}

$routerContainer = new Aura\Router\RouterContainer();
$map = $routerContainer->getMap();

// add a route to the map, and a handler for it
$map->get('api.read', '/api/', function ($request) {
    exit('XO-xo');
});

// Any implementation of PSR-15 MiddlewareInterface

var_dump($response);
**/